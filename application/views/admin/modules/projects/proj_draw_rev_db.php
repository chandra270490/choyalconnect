<section id="main-content">
  <section class="wrapper"> 
    <div class="row">
        <div class="col-lg-12">
            <h3><i class="fa fa-laptop"></i>Drawing Revision</h3>
            <?php require_once(APPPATH."views/admin/breadcrumb.php"); ?>
        </div> 
    </div>
    
    <div class="row" style="text-align:center">
        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/projectsc/proj_draw_cr_list">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Drawing Revision List<br/><br/>
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/projectsc/proj_draw_appl1_list">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Drawing Approval L1<br/><br/>
            </a>
        </div>

        <div class="col-lg-2">
        	<a href="<?php echo base_url(); ?>index.php/projectsc/proj_draw_appl2_list">
                <img src="<?php echo base_url(); ?>assets/admin/db/updated/flour_mill_dashboard.png" width="50%"/><br><br>
                Drawing Approval L2<br/><br/>
            </a>
        </div>

    </div><br>

  </section>
</section>