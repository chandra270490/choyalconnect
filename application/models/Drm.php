<?php
class Drm extends CI_Model{  
	function __construct(){   
		parent::__construct();  
	}

	function ListHead($tbl_nm){
		$query = $this->db->query("SHOW columns FROM $tbl_nm where Field not in('password','admin_pass')");

		return $query;
	}

	public function get_dr_by_id($dr_id){
		$query = $this->db->query("select * from dr_mst where dr_id = '".$dr_id."'");
		return $query;
	}

	//Dr Entry
	public function dr_entry($data){    
		$username = $_SESSION['username'];
		$dr_id = $this->input->post("dr_id");
		$dr_id1 = $this->input->post("dr_id");

		$dr_date = $this->input->post("dr_date");
		$dr_details = $this->input->post("dr_details");
		
		$arr_cnt = count($dr_details);
		
		//Transaction Start
		$this->db->trans_start();

		if($dr_id1 == ""){
			//Insert Code
			$sql = "insert into dr_mst(dr_date, dr_created_by) 
			values 
			('".$dr_date."', '".$username."')";

			$this->db->query($sql);

			for($i=0; $i<$arr_cnt; $i++){

				$sql_max_drid = "select max(dr_id) as dr_id from dr_mst where dr_created_by = '".$username."'";
				$qry_max_drid = $this->db->query($sql_max_drid)->row();
				$dr_id = $qry_max_drid->dr_id;

				$sql_itm_ins = "insert into dr_details(dr_id, dr_details) 
				values ('".$dr_id."', '".$dr_details[$i]."')";

				$qry_itm_ins = $this->db->query($sql_itm_ins);
			}

		} else {
			//Update Code
			$sql = "update dr_mst set dr_date = '".$dr_date."', dr_created_by = '".$username."'
			where dr_id = '".$dr_id."'";

			$this->db->query($sql);

			$sql_itm_cnt = "select count(*) as cnt from dr_details where dr_id = '".$dr_id."'";
			$qry_itm_cnt = $this->db->query($sql_itm_cnt)->row();
			$cnt = $qry_itm_cnt->cnt;

			if($cnt > 0){
				$sql_itm_del = "delete from dr_details where dr_id = '".$dr_id."'";
				$qry_itm_del = $this->db->query($sql_itm_del);

				for($i=0; $i<$arr_cnt; $i++){

					$sql_itm_ins = "insert into dr_details(dr_id, dr_details) 
                    values ('".$dr_id."', '".$dr_details[$i]."')";

                    $qry_itm_ins = $this->db->query($sql_itm_ins);

				}
			} else {
				for($i=0; $i<$arr_cnt; $i++){

					$sql_itm_ins = "insert into dr_details(dr_id, dr_details) 
                    values ('".$dr_id."', '".$dr_details[$i]."')";

                    $qry_itm_ins = $this->db->query($sql_itm_ins);

				}
			}

		}

		$this->db->trans_complete();
		//Transanction Complete
	 }
}  
?>